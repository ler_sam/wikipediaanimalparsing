import json
import logging

from crawling_data.html_read import HtmlReader
from crawling_data.download_animal_image import DownloadAnimalImage
from crawling_data.crawling_taxon_table import CrawlingTaxonTable

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


def get_page_data(debug=False) -> str:
    """
    retrieve `wiki List_of_animal_names` html data
    :param debug:bool load static file for testing or read from
    https://en.wikipedia.org/wiki/List_of_animal_names
    :return:
    """
    if debug:
        path = 'testing/list_of_animal_names.html'
        with open(path, encoding='utf8') as f:
            page_data = f.read()
    else:
        wiki_link = 'https://en.wikipedia.org/wiki/List_of_animal_names'
        html_reader = HtmlReader()
        page_data = html_reader.get_page_data(wiki_link)

    return page_data


def grouping_collateral_adjective():
    """
    grouping all animals by `collateral adjectives` column
    :return: json string - animal list
    """
    page_data = get_page_data()
    if page_data:
        crawling_table = CrawlingTaxonTable(page_data)
        grouping_data = crawling_table.get_animal_list(grouping='collateral_adjective')
        data = {}
        for key, animals in grouping_data.items():
            data[key] = list(map(lambda animal: animal.to_dict(), animals))

        return json.dumps(data, sort_keys=True)


def download_animals_images():
    """
    download all animals images from wiki page
    :return:
    """
    page_data = get_page_data()

    if page_data:
        crawling_table = CrawlingTaxonTable(page_data)
        animals_data = crawling_table.get_animal_list()
        animals_list = [{"name": item.title, "summary_url": item.get_summary_url()} for item in animals_data]
        download_image = DownloadAnimalImage(animals_list)
        download_image.wrapper()
        logging.debug('finished download images')


def create_html_file(animals: str) -> None:
    """
    creates formatting html file, path 'external/animals.html'
    :param animals:
    :return: 
    """
    def check_single_entry(entry) -> str:
        if isinstance(entry, list):
            return '<br />'.join(entry)
        return entry

    logging.debug('start creating html files')
    path = 'external/page_template.html'
    result_path = 'external/animals.html'
    page_data = ''
    animals = json.loads(animals)
    for key, animal_list in animals.items():
        for single_animal in animal_list:
            name = single_animal.get('animal')
            if isinstance(name, list):
                name = ' '.join(name)

            young = check_single_entry(single_animal.get('young'))
            female = check_single_entry(single_animal.get('female'))
            male = check_single_entry(single_animal.get('male'))
            collective_noun = check_single_entry(single_animal.get('collective_noun'))
            collateral_adjective = check_single_entry(single_animal.get('collateral_adjective'))
            culinary_noun_for_meat = check_single_entry(single_animal.get('culinary_noun_for_meat'))
            page_data += f"<tr>" \
                         f"<td>{name}</td>" \
                         f"<td>{young}</td>" \
                         f"<td>{female}</td>" \
                         f"<td>{male}</td>" \
                         f"<td>{collective_noun}</td>" \
                         f"<td>{collateral_adjective}</td>" \
                         f"<td>{culinary_noun_for_meat}</td>" \
                         f"</tr>"

    with open(path, encoding='utf8') as template, open(result_path, encoding='utf8', mode='w') as output:
        page_tamlate = template.read()
        animal_page = page_tamlate.replace('{{animals_rows}}', page_data)
        output.write(animal_page)
        logging.debug(f'created html file in: {result_path}')


if __name__ == '__main__':
    logging.info('starting')
    data = grouping_collateral_adjective()
    create_html_file(data)
    download_animals_images()
    logging.info('finished')
