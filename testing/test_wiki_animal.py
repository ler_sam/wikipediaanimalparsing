import ast
import os
import json

import unittest

from crawling_data.crawling_taxon_table import CrawlingTaxonTable


class WikiListOfAnimalNames(unittest.TestCase):
    def setUp(self):
        path = 'list_of_animal_names.html'
        page_data = self.load_static_page_data(path)
        self.taxon_table = CrawlingTaxonTable(page_data)

    def load_static_page_data(self, path) -> str:
        """
        loading file data
        :param path:
        :return:
        """
        type = os.path.splitext(path)[1]

        with open(path, encoding='utf8') as f:
            if type in ['.html', '.txt']:
                page_data = f.read()
            elif type == '.json':
                page_data = json.load(f)

        return page_data

    def tearDown(self) -> None:
        del self.taxon_table

    def test_grouping_by_female(self):
        """
        checking grouping by `female` column
        :return:
        """
        actual = self.taxon_table.get_animal_list(grouping='female')
        actual_data = {}
        for key, animals in actual.items():
            actual_data[key] = list(map(lambda animal: animal.to_dict(), animals))

        expected = self.load_static_page_data('expected_results/grouping_by_female.json')

        self.assertDictEqual(actual_data, expected)

    def test_grouping_by_collateral_adjective(self):
        """
        checking grouping by `female` column
        :return:
        """
        actual = self.taxon_table.get_animal_list(grouping='female')
        actual_data = {}
        for key, animals in actual.items():
            actual_data[key] = list(map(lambda animal: animal.to_dict(), animals))

        expected = self.load_static_page_data('expected_results/collateral_adjective.json')

        self.assertDictEqual(actual_data, expected)

    def test_download_animal_image(self):
        """
        checking if download succeeded
        :return:
        """
        from pathlib import Path
        antelope_img = Path('/tmp/Antelope.jpg')
        self.assertTrue(antelope_img.exists())


if __name__ == '__main__':
    unittest.main()
