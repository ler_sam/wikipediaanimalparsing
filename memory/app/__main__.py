import os
import time
import psutil
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

def statistic(func):
    def wrapper(*arg, **kwargs):
        process = psutil.Process(os.getpid())
        process.cpu_percent()

        start_time = time.time()
        result = func(*arg, **kwargs)
        end_time = time.time()

        load1, load5, load15 = psutil.getloadavg()
        cpu_usage = process.cpu_percent()
        memory_usage = process.memory_info().rss / (1024 * 1024)

        execution_time = end_time - start_time
        log.info("run_time: %.3f sec, load_average: %.3f, %.3f, %.3f, cpu_usage: %.3f%%, memory_usage: %.3f%%",
                 (end_time - start_time), load1, load5, load15, cpu_usage, memory_usage)

        return result

    return wrapper


@statistic
def run_code():
    for i in range(1_000_000_000):
        _ = i * i


if __name__ == "__main__":
    run_time = 10 * 60  # 10 min
    start_time = time.time()
    end_time = time.time()
    while time.time() - start_time < run_time:
        run_code()

    end_time = time.time()
    log.info("Program execution completed: %.3f", end_time - start_time)
