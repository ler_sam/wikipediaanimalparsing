#!/bin/bash

#while true; do
#  top -b -n1 | awk '{print $9,$0}' | sort -n -r | cut -d' ' -f2- |
#    awk '/^%Cpu\(s\)/ {
#      split($2, arr, ",");
#      cpu_usage = arr[1];
#      gsub("%", "", cpu_usage);
#      print "CPU Usage:", cpu_usage;
#    }' 2>>&1 |tee top_stats.log
#
#  sleep 10
#done
#

#!/bin/bash

# Filter string (e.g., "u" for user processes)
filter_string="python"

# Function to get CPU usage
get_cpu_usage() {
  top | grep "python" |
    awk '/^%Cpu\(s\)/ {
      split($2, arr, ",");
      cpu_usage = arr[1];
      gsub("%", "", cpu_usage);
      print cpu_usage
    }'
}

# Function to get memory usage
get_memory_usage() {
  free -m | awk '/Mem:/ { print $3 }'
}

while true; do
  # Get CPU and memory usage
  current_cpu=$(get_cpu_usage)
  current_memory=$(get_memory_usage)

  # Log results
  echo "$(date '+%Y-%m-%d %H:%M:%S') - CPU Usage: $current_cpu%" >> top_stats.log
  echo "$(date '+%Y-%m-%d %H:%M:%S') - Memory Usage: $current_memory MB" >> top_stats.log

  sleep 10
done