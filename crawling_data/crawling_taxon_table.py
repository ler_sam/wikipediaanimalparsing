from bs4 import BeautifulSoup
from animal.animal_model import AnimalModel


class CrawlingTaxonTable(object):
    """
    getting data from `Terms by species or taxon` table
    """

    def __init__(self, page_data):
        self.page_data = page_data
        self.animal_list: list[AnimalModel] = []

    def _crawling_data(self):
        """
        we have two `wikitable` on page wee need the second one
        :return:
        """
        soup = BeautifulSoup(self.page_data, "html.parser")
        wikitables = soup.findAll('table', class_="wikitable")
        if len(wikitables) < 2:
            # missing table or html changed
            raise ValueError('missing wikitable `Terms by species or taxon` or html changed')

        self._species_or_taxon_table(wikitables[1])

    def _species_or_taxon_table(self, table_data):
        """
        crawling and collecting animals
        :param table_data:
        :return:
        """
        for single_row in table_data.findChildren(['tr']):
            cel = single_row.findChildren(['td'])
            if cel and len(cel) == 7:
                curr_animal = self._filtering_single_animal(cel)
                self.animal_list.append(curr_animal)

    def _filtering_single_animal(self, line_result) -> AnimalModel:
        """
        filtering single row <tr>
        :param line_result:
        :return: AnimalModel
        """

        def filter_single_value(in_val):
            """
            clearing single cell(<td>) text data
            :param in_val:
            :return:
            """
            for reference in in_val.find_all('sup', class_="reference"):
                reference.clear()

            for reference in in_val.find_all('i'):
                reference.clear()

            for reference in in_val.find_all('br'):
                reference.replace_with('|*|')

            curr_val = in_val.get_text().strip() if in_val.string else None

            if not curr_val:
                curr_val = in_val.text.strip() if in_val.text else None

            if curr_val and '|*|' in curr_val:
                curr_val = list(map(lambda val: val.strip(), filter(None, curr_val.split('|*|'))))
                if len(curr_val) == 1:
                    curr_val = curr_val[0]

            return curr_val

        row_keys = ['animal', 'young', 'female', 'male', 'collective_noun', 'collateral_adjective',
                    'culinary_noun_for_meat']
        animal_data = {}
        for cell_id, cell_data in enumerate(line_result):
            single_value = filter_single_value(cell_data)
            animal_data[row_keys[cell_id]] = single_value
            if row_keys[cell_id] == 'animal':
                title = cell_data.find('a').get('title')
                animal_data['title'] = title

        return AnimalModel(**animal_data)

    def _animal_grouping(self, group_key) -> dict:
        """
        preform grouping on load page data
        :param group_key:
        :return:
        """
        data = {}
        for animal in self.animal_list:
            grouping_key = animal.__dict__[group_key]
            if grouping_key is None:
                continue

            if isinstance(grouping_key, str):
                grouping_key = [grouping_key]

            for key in grouping_key:
                if key not in data:
                    data[key] = []
                data[key].append(animal)

        return data

    def get_animal_list(self, refreshing=False, grouping=None):
        """
        main function
        :param refreshing:
        :param grouping:
        :return:
        """
        if not self.animal_list or refreshing:
            self._crawling_data()
        if grouping:
            return self._animal_grouping(grouping)

        return self.animal_list
