#!/usr/bin/env python
# -*- coding: utf8 -*-

import requests


class HtmlReader(object):
    """
    puling page Data
    """

    def __init__(self):
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                          "Chrome/96.0.4664.45 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.8",
            "Accept-Encoding": "gzip, deflate, br"}

    def get_page_data(self, site_url) -> str:
        """
        retrieve html page data
        :param site_url:
        :return:
        """
        page_data = ''
        s = requests.Session()
        req = requests.Request('GET', site_url, headers=self.headers)
        prepped = s.prepare_request(req)
        resp = s.send(prepped)
        if resp.status_code == requests.codes.ok:
            if resp.encoding in ['UTF-8', 'utf-8']:
                page_data = resp.text
            else:
                page_data = resp.text.decode("utf8")
        else:
            raise requests.HTTPError()

        resp.close()
        return page_data
