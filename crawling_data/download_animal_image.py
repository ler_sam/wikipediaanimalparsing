import os
import json
import re
import logging

from urllib.request import urlretrieve
from jmespath import search
from time import sleep

from crawling_data.html_read import HtmlReader

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


class DownloadAnimalImage(object):
    """
    download Animal Images from `wikipedia`
    the image link is in summary page
    open summary page for each animal, extract link from `originalimage.source` and downloading
    """
    def __init__(self, animals_list: list):
        self.animals_list = animals_list
        self.animal_links = {}

    def _extract_image_link(self, animal=None):
        """
        open wikipedia summary page and gets 'originalimage'
        :return:
        """
        if animal is None:
            raise ValueError('Empty animal name')

        html_reader = HtmlReader()
        try:
            summary_url = animal.get('summary_url')
            page = html_reader.get_page_data(summary_url)
            if page:
                page = json.loads(page)
                image_src = search('originalimage.source', page)
                if image_src:
                    animal_name = animal.get('name')
                    animal_name = re.sub("[^0-9a-zA-Z]+", "1", animal_name)
                    logging.debug(f'name:`{animal_name}`, link: {image_src}')
                    self.animal_links[animal_name] = image_src
        except Exception as err:
            logging.error(err)

    def _download_animal_image(self):
        """
        downloading image from wikipedia.org and saving
        """
        directory = '/tmp'
        for name, link in self.animal_links.items():
            logging.debug(f'start downloading: {name} link:{link}')
            type = os.path.splitext(link)[1]
            urlretrieve(link, f'{directory}/{name}{type}')

            logging.debug(f'finish downloading: {name}')
            sleep(1)

    def wrapper(self):
        """
        main wrapper to download animal image
        :return:
        """
        logging.debug('starting download process')
        for animal in self.animals_list:
            self._extract_image_link(animal)
        self._download_animal_image()
        logging.debug('end download process')
