import json
from typing import Union


class AnimalModel(object):
    """
    single animal as in `Terms by species or taxon` table
    """

    def __init__(self, animal: str, young: str, female: str, male: str, collective_noun: list, collateral_adjective: str,
                 culinary_noun_for_meat: str, title: str):
        self.animal = animal
        self.young = young
        self.female = female
        self.male = male
        self.collective_noun = collective_noun
        self.collateral_adjective = collateral_adjective
        self.culinary_noun_for_meat = culinary_noun_for_meat
        self.title = title

    def get_summary_url(self) -> Union[str, None]:
        """
        retrive url to animal summary page
        :return:
        """
        return f"https://en.wikipedia.org/api/rest_v1/page/summary/{self.title}" if self.title else None

    def __repr__(self):
        return json.dumps(self.__dict__)

    def to_dict(self):
        return self.__dict__
